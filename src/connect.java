
import static java.lang.System.exit;
import java.sql.*;
import java.util.Scanner;


public class connect {
    
private Connection con;
private Statement st;
private ResultSet rs;
    
    


public connect(){

    try{
    Class.forName("com.mysql.jdbc.Driver");
    con = DriverManager.getConnection("jdbc:mysql://localhost:3306/elvis ab db", "root", "");
    st = con.createStatement();
    }
    catch(Exception ex){
    
       System.out.println("Error "+ex);
    }
}
public void getAnst(){
    
    try{
    String query = "SELECT * FROM anst";
    rs = st.executeQuery(query);
    System.out.println("Records from Database");
    
    while(rs.next()){
        
    int AnstNr = rs.getInt("AnstNr");
    String Nanm = rs.getString("Nanm");
    String Adress = rs.getString("Adress");
    int Telefon = rs.getInt("Telefon");
    String Email = rs.getString("Email");
    String Yrkesroll = rs.getString("Yrkesroll");
    
    System.out.println("Anställningsnummer: " + AnstNr + " Namn: "+ Nanm + " Adress: "+Adress+" Telefon: "+Telefon+" Email: "+Email+" Yrkesroll: "+Yrkesroll);
    }
    }
    catch(Exception ex){
        System.out.println("Error "+ex);
    }
}
public void getKunder(){
    
    try{
    String query = "SELECT * FROM kunder";
    rs = st.executeQuery(query);
    System.out.println("Records from Database");
    
    while(rs.next()){
    
    int OrgNr = rs.getInt("OrgNr");
    String Namn = rs.getString("Namn");
    String Adress = rs.getString("Adress");
    int TelNr = rs.getInt("TelNr");
    String Email = rs.getString("Email");
    String Kontaktperson = rs.getString("KontaktPerson");
    
    System.out.println("OrgNr: "+OrgNr+" Namn: "+Namn+" Adress: "+Adress+" TelNr: "+TelNr+" Email: "+Email+" Kontaktperson: "+Kontaktperson);
    
        
    }
    
        
    }
    catch(Exception ex){
        System.out.println("Error "+ex);
        
    }
}

public void getProjekt(){
    
    try{
    String query = "SELECT * FROM projekt";
    rs = st.executeQuery(query);
    System.out.println("Records from Database");
    
    while(rs.next()){
        
    int projektnr = rs.getInt("ProjektNr");
    String projektnamn = rs.getString("Projektnamn");
    String adress = rs.getString("Adress");
    String kontaktperson = rs.getString("Kontaktperson");
    String arbetsledare = rs.getString("Arbetsledare");
    
    System.out.println("Projektnummer: " + projektnr + " Projektamn: "+ projektnamn + " Adress: " + adress + " Kontaktperson: " + kontaktperson + " Arbetsledare: " + arbetsledare);
    }
    }
    catch(Exception ex){
        System.out.println("Error "+ex);
    }
}
    
  public void tableSelect(){
        
    Scanner scan = new Scanner(System.in);    
    
    System.out.println("Skriv (1) för att visa Anställda");
    System.out.println("Skriv (2) för att visa Kunder");
    System.out.println("Skriv (3) för att visa Projekt");
    System.out.println("Skriv (0) för att avsluta");
    System.out.println("================================");
    
    int read = scan.nextInt();
    
    if(read==1){
    getAnst();
    tableSelect();
    
    
    }
    else if(read==2){
    getKunder();
    tableSelect();
    }
    else if(read==3){
    getProjekt();
    tableSelect();
    }
    else if(read==0){
    exit(1);
    }
    else{
        System.out.println("Error! Välj Du har inte valt något från menyvalen");
        tableSelect();
    }
    }   
    
}

